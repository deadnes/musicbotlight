package com.deadnes.musicbotlight;

import uk.co.caprica.vlcj.discovery.NativeDiscovery;

public class MusicBotLightApplication {
	public static void main (String args[]) {
		new NativeDiscovery().discover();
		MusicBot.run();
	}
}
