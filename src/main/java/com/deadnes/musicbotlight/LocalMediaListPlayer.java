package com.deadnes.musicbotlight;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import uk.co.caprica.vlcj.component.AudioMediaPlayerComponent;
import uk.co.caprica.vlcj.medialist.MediaList;
import uk.co.caprica.vlcj.medialist.MediaListItem;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;

public final class LocalMediaListPlayer {

	private static final AudioMediaPlayerComponent mediaPlayerComponent = new AudioMediaPlayerComponent();
	private static final MediaList mediaList = mediaPlayerComponent.getMediaPlayerFactory().newMediaList();
	private int currentItemIndex = -1;
	private String playlistsDir = ".\\playlists";
	private String currentPlaylistName = "Default";
	private static LocalMediaListPlayer instance = null;

	private LocalMediaListPlayer () {
		initialize();
	}

	public static LocalMediaListPlayer getInstance (){
		if (instance == null) {
			instance = new LocalMediaListPlayer();
		}
		return instance;
	}

	public void loadConfig () {
		List <String> options = new ArrayList<>();
		try {
			options = Files.readAllLines(Paths.get("config.ini"));
			String temp [];
			for (String option : options) {
				if (option.startsWith("playlists_folder")) {
					temp = option.split("=\\s*",2);
					if (temp.length == 2) {
						playlistsDir = temp[1];
					}
				}
				if (option.startsWith("resume_playlist_state")) {
					temp = option.split("=\\s*", 2);
					if (temp.length == 2) {
						if (temp[1].equals("true")) {
							try {
								List <String> state = new ArrayList<>();
								state = Files.readAllLines(Paths.get("playlist.state"));
								currentPlaylistName = state.get(0);
								loadPlaylistFromFile(currentPlaylistName);
								currentItemIndex = Integer.parseInt(state.get(1));
								play(currentItemIndex);
							}catch(IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void initialize () {
		mediaPlayerComponent.getMediaPlayer().setAudioOutput("waveout");
		mediaPlayerComponent.getMediaPlayer().setAudioOutputDevice(null, "Line 1 (Virtual Audio Cable) ($1,$64)");
		mediaPlayerComponent.getMediaPlayer().setStandardMediaOptions("novideo");
		mediaPlayerComponent.getMediaPlayer().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
			@Override
			public void playing(MediaPlayer mediaPlayer) {
				if (mediaPlayer.subItemCount() > 0) {
					System.out.println("Playing..." + mediaPlayer.getSubItemMediaMetaData().get(0).getTitle());
					MusicBot.setNowPlaying(mediaPlayer.getSubItemMediaMetaData().get(0).getTitle());
					MusicBot.setSource("[URL]" + mediaPlayer.mrl() + "[/URL]");
				}else {
					System.out.println("Playing..." + mediaPlayer.getMediaMetaData().getTitle());
					MusicBot.setNowPlaying(mediaPlayer.getMediaMetaData().getTitle());
					MusicBot.setSource("local file");
				}
			}
			@Override
			public void paused(MediaPlayer mediaPlayer) {
				System.out.println("Paused...");
			}
			@Override
			public void stopped(MediaPlayer mediaPlayer) {
				System.out.println("Stopped...");
				mediaPlayerComponent.getMediaPlayer().setAudioOutput("waveout");
				mediaPlayerComponent.getMediaPlayer().setAudioOutputDevice(null, "Line 1 (Virtual Audio Cable) ($1,$64)");
				playNext();
			}
			@Override
			public void finished(MediaPlayer mediaPlayer) {
				System.out.println("Finished...");
				if (mediaPlayer.subItemCount() > 0 && mediaPlayer.subItemIndex() == -1) {
					mediaPlayer.playNextSubItem();
				}else {
					stop();
				}
			}
			@Override
			public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {
				System.out.println("Title changed...");
				System.out.println("New title = " + newTitle);
			}
			@Override
			public void error(MediaPlayer mediaPlayer) {
				System.out.println("Error...");
			}
			@Override
			public void newMedia(MediaPlayer mediaPlayer) {
				System.out.println("New Main Media Opened..." + mediaPlayer.getMediaMetaData().getTitle());
			}
			@Override
			public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {
				System.out.println("New Sub Item Playback Begun...");
				System.out.println("Sub Item Index = " + subItemIndex);
			}
			@Override
			public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {
				System.out.println("Sub Item Finished...");
				System.out.println("Sub Item Index = " + subItemIndex);
			}
		});
	}

	public void play (String mrl) {
		mediaPlayerComponent.getMediaPlayer().playMedia(mrl);
	}

	public void play (int songNumber) {
		if (mediaList.size() > 0 && songNumber > -1 && songNumber < mediaList.size()) {
			currentItemIndex = songNumber;
			mediaPlayerComponent.getMediaPlayer().playMedia(mediaList.items().get(currentItemIndex).mrl());
		}
	}

	public void playNext () {
		if (mediaList.size() > 0) {
			currentItemIndex++;
			if (mediaList.size() == currentItemIndex) {
				currentItemIndex = 0;
			}
			mediaPlayerComponent.getMediaPlayer().playMedia(mediaList.items().get(currentItemIndex).mrl());
		}
	}

	public void playPrevious () {
		if (mediaList.size() > 0) {
			currentItemIndex--;
			if (currentItemIndex <= -1) {
				currentItemIndex = mediaList.size() - 1;
			}
			mediaPlayerComponent.getMediaPlayer().playMedia(mediaList.items().get(currentItemIndex).mrl());
		}
	}

	public void savePlaylistToFile (String playlistName) {
		try {
			if (!Files.exists(Paths.get(playlistsDir))) {
				Files.createDirectory(Paths.get(playlistsDir));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			List <String> items = new ArrayList<>();
			for (MediaListItem item : mediaList.items()) {
				items.add(item.mrl());
			}
			Files.write(Paths.get(playlistsDir, playlistName + ".playlist"), items);
			currentPlaylistName = playlistName;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadPlaylistFromFile (String playlistName) {
		List <String> items = new ArrayList<>();
		try {
			items = Files.readAllLines(Paths.get(playlistsDir, playlistName + ".playlist"));
			currentItemIndex = -1;
			mediaList.clear();
			for (String item : items) {
				mediaList.addMedia(item);
			}
			currentPlaylistName = playlistName;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadPlaylistFromFIle (int playlistNumber) {
		List<Path> names;
		try {
			names = Files.list(Paths.get(playlistsDir)).collect(Collectors.toList());
			int itemIndex = 0;
			for (Path name : names) {
				if (name.getFileName().toString().endsWith(".playlist")) {
					itemIndex++;
					if (itemIndex == playlistNumber) {
						loadPlaylistFromFile(name.getFileName().toString().substring(0, name.getFileName().toString().length() - 9));
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void stop () {
		mediaPlayerComponent.getMediaPlayer().stop();
	}

	public void pause () {
		mediaPlayerComponent.getMediaPlayer().pause();
	}

	public void resume () {
		mediaPlayerComponent.getMediaPlayer().play();
	}

	public void addToPlaylist (String mrl) {
		mediaList.addMedia(mrl);
	}

	public void removeFromPlaylist (int mediaIndex) {
		if (mediaIndex > -1 && mediaIndex < mediaList.size()) {
			mediaList.removeMedia(mediaIndex);	
		}
	}

	public void saveState () {
		try {
			List<String> state = new ArrayList<>();
			state.add(currentPlaylistName);
			state.add(String.valueOf(currentItemIndex));
			Files.write(Paths.get("playlist.state"),  state);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exit () {
		saveState();
		mediaPlayerComponent.release();
		System.exit(0);
	}

	public String getTitle () {
		if (mediaPlayerComponent.getMediaPlayer().getMediaMetaData().getTitle().startsWith("watch?v=")) {
			while(mediaPlayerComponent.getMediaPlayer().getSubItemMediaMetaData().isEmpty());
			return mediaPlayerComponent.getMediaPlayer().getSubItemMediaMetaData().get(0).getTitle();
		}else {
			return mediaPlayerComponent.getMediaPlayer().getMediaMetaData().getTitle();
		}
	}

	public String getPlaylistNames () {
		String formattedNames = "Playlists:";
		List<Path> names;
		try {
			names = Files.list(Paths.get(playlistsDir)).collect(Collectors.toList());
			int itemIndex = 0;
			for (Path name : names) {
				if (name.getFileName().toString().endsWith(".playlist")) {
					itemIndex++;
					formattedNames += "\n" + itemIndex + ": " + name.getFileName().toString().substring(0, name.getFileName().toString().length() - 9);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return formattedNames;
	}

	public String getCurrentPlaylistName () {
		return currentPlaylistName;
	}

	public int getCurrentItemIndex () {
		return currentItemIndex;
	}

	public String peekPlaylistItemNames() {
		if (mediaList.size() == 0) {
			return "Empty Playlist";
		}
		String formattedNames = currentPlaylistName + ":";
		int itemIndex = 0;
		for (MediaListItem item : mediaList.items()) {
			if (itemIndex >= currentItemIndex-5 && itemIndex <= currentItemIndex+5) {
				formattedNames += "\n" + (itemIndex+1) + ": ";
				if (itemIndex == currentItemIndex) {
					formattedNames += "[b]";
				}
				if (item.name().startsWith("watch?v=")){
					if (item.subItems().isEmpty()) {
						formattedNames += "[URL]" + item.mrl() + "[/URL]";
					}else {
						formattedNames += item.subItems().get(0).name();
					}
				}else {
					formattedNames += item.name();
				}
				if (itemIndex == currentItemIndex) {
					formattedNames += "[/b]";
				}
			}
			itemIndex++;
			if (itemIndex > currentItemIndex+5) {
				break;
			}
		}
		return formattedNames;
	}

	public String getPlaylistItemNames(){
		if (mediaList.size() == 0) {
			return "Empty Playlist";
		}
		String formattedNames = currentPlaylistName + ":";
		int itemIndex = 0;
		for (MediaListItem item : mediaList.items()) {
			formattedNames += "\n" + (itemIndex+1) + ": ";
			if (itemIndex == currentItemIndex) {
				formattedNames += "[b]";
			}
			if (item.name().startsWith("watch?v=")){
				if (item.subItems().isEmpty()) {
					formattedNames += "[URL]" + item.mrl() + "[/URL]";
				}else {
					formattedNames += item.subItems().get(0).name();
				}
			}else {
				formattedNames += item.name();
			}
			if (itemIndex == currentItemIndex) {
				formattedNames += "[/b]";
			}
			itemIndex++;
		}
		return formattedNames;
	}
}
