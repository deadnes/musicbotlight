package com.deadnes.musicbotlight;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventType;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.reconnect.ConnectionHandler;
import com.github.theholywaffle.teamspeak3.api.reconnect.ReconnectStrategy;


public final class MusicBot {
	
	private static volatile int clientId;
	private static LocalMediaListPlayer localMediaListPlayer = LocalMediaListPlayer.getInstance();
	
	private static String nowPlaying = "nothing";
	private static String source = "nothing";
	
	private MusicBot() {
	}
	
	public static void setSource (String newSource) {
		source = newSource;
	}
	
	public static void setNowPlaying (String newNowPlaying) {
		nowPlaying = newNowPlaying;
	}
	
	private static void initialize (final TS3Api api) {
		api.sendChannelMessage("MusicBot is online!");
		api.addTS3Listeners(new TS3EventAdapter() {
			@Override
			public void onTextMessage (TextMessageEvent event) {
				if (event.getTargetMode() == TextMessageTargetMode.CHANNEL && event.getInvokerId() != clientId) {
					String message = event.getMessage();
					if (message.startsWith("!")) {
						String [] messages = message.split("\\s+", 2);
						String command = messages[0];
						String song;
						String playlistName;
						switch(command) {
						default:
							api.sendChannelMessage("Unknown command.");
							break;
						case "!help":
							api.sendChannelMessage("Available commands are:\n"
									+ "!play SONG - Plays song by item NUMBER or youtube LINK\n"
									+ "!pause - Pauses playback\n"
									+ "!resume - Resumes playback\n"
									+ "!source - Gets URL of current song\n"
									+ "!name - Gets name of current song\n"
									+ "!add LINK - Adds song to playlist by youtube LINK\n"
									+ "!remove NUMBER - Removes song with corresponding item NUMBER from list\n"
									+ "!next - Plays next song in playlist\n"
									+ "!back - Plays previous song in playlist\n"
									+ "!list - List all songs in current playlist\n"
									+ "!peek - List songs close to the current one in playlist\n"
									+ "!playlists - Show all saved playlists\n"
									+ "!save NAME - Saves current playlist as NAME\n"
									+ "!load NAME - Loads playlist called NAME\n"
									+ "!help - Displays this menu");
							break;
						case "!play":
							if (messages.length == 1) {
								localMediaListPlayer.playNext();
								break;
							}
							song = messages[1];
							if (song.startsWith("[URL]")) {
								source = song;
								song = song.substring(5, song.length()-6);
								localMediaListPlayer.play(song);
								api.sendChannelMessage("Now Playing: " + (nowPlaying = localMediaListPlayer.getTitle()) );
								break;
							}
							try {
								int songNumber = Integer.parseInt(messages[1]);
								if (songNumber < 0) {
									songNumber = -songNumber;
								}
								localMediaListPlayer.play(songNumber - 1);
								api.sendChannelMessage("Now Playing: " + (nowPlaying = localMediaListPlayer.getTitle()));
							}catch (NumberFormatException e) {
								api.sendChannelMessage("Not a valid item number!");
								break;
							}
							break;
						case "!add":
							if (messages.length == 1) {
								api.sendChannelMessage("Add what?");
								break;
							}
							song = messages[1];
							if (song.startsWith("[URL]")) {
								song = song.substring(5, song.length()-6);
								localMediaListPlayer.addToPlaylist(song);
								api.sendChannelMessage("Item added to playlist");
								break;
							}
							break;
						case "!remove":
							if (messages.length == 1) {
								api.sendChannelMessage("Remove song with what number?");
								break;
							}
							try {
								int songNumber = Integer.parseInt(messages[1]);
								if (songNumber < 0) {
									songNumber = -songNumber;
								}
								localMediaListPlayer.removeFromPlaylist(songNumber - 1);
								api.sendChannelMessage("Item #" + songNumber + " removed from playlist");
							}catch (NumberFormatException e) {
								api.sendChannelMessage("Not a valid item number!");
								break;
							}
							break;
						case "!next":
							localMediaListPlayer.playNext();
							break;
						case "!back":
							localMediaListPlayer.playPrevious();
							break;
						case "!list":
							api.sendChannelMessage(localMediaListPlayer.getPlaylistItemNames());
							break;
						case "!peek":
							api.sendChannelMessage(localMediaListPlayer.peekPlaylistItemNames());
							break;
						case "!playlists":
							api.sendChannelMessage(localMediaListPlayer.getPlaylistNames());
							break;
						case "!save":
							if (messages.length == 1) {
								api.sendChannelMessage("Save as what?");
								break;
							}
							playlistName = messages[1];
							localMediaListPlayer.savePlaylistToFile(playlistName);
							api.sendChannelMessage("Saving playlist as " + playlistName);
							break;
						case "!load":
							if (messages.length == 1) {
								api.sendChannelMessage("Load which one?");
								break;
							}
							try {
								int playlistNumber = Integer.parseInt(messages[1]);
								if (playlistNumber < 0) {
									playlistNumber = -playlistNumber;
								}
								localMediaListPlayer.loadPlaylistFromFIle(playlistNumber);
								api.sendChannelMessage("Loading playlist #" + playlistNumber);
							}catch (NumberFormatException e) {
								playlistName = messages[1];
								localMediaListPlayer.loadPlaylistFromFile(playlistName);
								api.sendChannelMessage("Loading playlist " + playlistName);
								break;
							}
							break;
						case "!pause":
							api.sendChannelMessage("Playback paused");
							localMediaListPlayer.pause();
							break;
						case "!resume":
							api.sendChannelMessage("Playback resumed");
							localMediaListPlayer.resume();
							break;
						case "!source":
							api.sendChannelMessage(source);
							break;
						case "!name":
							api.sendChannelMessage(nowPlaying);
							break;
						case "!kill":
							if (!event.getInvokerName().equals("Deadnes")) {
								api.sendChannelMessage("You can't kill me, I'm invicible!");
								break;
							}
							api.sendChannelMessage("Shutting down");
							api.logout();
							localMediaListPlayer.exit();
							break;
						}
					}
				}
			}
		});
	}
	
	private static void reconnectionInitialize (TS3Api api) {
		api.login("musicbot", "BBb6pGID");
		api.selectVirtualServerById(1);
		api.setNickname("MusicBot");
		api.moveQuery(2);
		api.registerEvent(TS3EventType.TEXT_CHANNEL, 0);
		clientId = api.whoAmI().getId();
		localMediaListPlayer.loadConfig();
	}
	
	public static void run() {
		
		final TS3Config config = new TS3Config();
		config.setHost("localhost");
		config.setReconnectStrategy(ReconnectStrategy.exponentialBackoff());
		config.setConnectionHandler(new ConnectionHandler() {
			
			@Override
			public void onConnect(TS3Query ts3Query) {
				reconnectionInitialize(ts3Query.getApi());
			}
			@Override
			public void onDisconnect(TS3Query ts3Query) {
				localMediaListPlayer.saveState();
			}
		});
		
		final TS3Query query = new TS3Query(config);
		query.connect();
		
		initialize(query.getApi());
	}
}
